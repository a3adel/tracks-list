package com.example.mondaitask

import android.app.Application
import com.example.mondaitask.Network.Manager.APIService
import com.example.mondaitask.Network.Manager.RemoteDataManager
import com.example.mondaitask.utils.CONTENT_TYPE_KEY
import com.example.mondaitask.utils.GATE_WAY_KEY_KEY

class MondaiApp : Application() {
    override fun onCreate() {
        super.onCreate()

    }


    private fun getAPIClient(): APIService {
        val basicHeaders = HashMap<String, String>()
        basicHeaders.put(CONTENT_TYPE_KEY, getString(R.string.content_type))
        basicHeaders.put(GATE_WAY_KEY_KEY, getString(R.string.gateway_key))
        val apiService = APIService.getInstance().addBasicHeaders(basicHeaders)
        return apiService
    }

    fun getRemoteDataManager(): RemoteDataManager {
        if (dataManager == null)
            dataManager = RemoteDataManager(getAPIClient())
        return dataManager as RemoteDataManager
    }

    companion object {
        private var dataManager: RemoteDataManager? = null
    }
}