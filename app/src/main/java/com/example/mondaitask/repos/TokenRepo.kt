package com.example.mondaitask.repos

import com.example.mondaitask.Network.Manager.RemoteResponse
import com.example.mondaitask.models.Token
import kotlinx.coroutines.flow.Flow

interface TokenRepo : BaseRepo {
    suspend fun getTokenFromServer(gateWaySubUrl: String = "/v0/api/gateway/token/client"): Flow<RemoteResponse<Token>>
    fun saveToken()
    fun fetchTokenFromCache()
}