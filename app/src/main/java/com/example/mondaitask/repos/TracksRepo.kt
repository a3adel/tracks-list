package com.example.mondaitask.repos

import android.graphics.Bitmap
import com.example.mondaitask.Network.Manager.RemoteResponse
import com.example.mondaitask.Network.image_loader.ImageLoaderResponse
import com.example.mondaitask.models.TracksResponse
import kotlinx.coroutines.flow.Flow

interface TracksRepo : BaseRepo {
    suspend fun getTracks(
        tracksUrl: String = "/v2/api/sayt/flat",
        key: String,
        parameters: HashMap<String, String>
    ): Flow<RemoteResponse<TracksResponse>>

}