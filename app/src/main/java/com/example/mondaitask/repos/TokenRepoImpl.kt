package com.example.mondaitask.repos

import com.example.mondaitask.Network.Manager.RemoteDataManager
import com.example.mondaitask.Network.Manager.RemoteResponse
import com.example.mondaitask.models.Token
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class TokenRepoImpl(val dataManager: RemoteDataManager) : TokenRepo {
    override suspend fun getTokenFromServer(gateWaySubUrl: String): Flow<RemoteResponse<Token>> {
        return flow { emit(dataManager.getTokenFromServer(gateWaySubUrl)) }.flowOn(Dispatchers.IO)
    }


    override fun saveToken() {

    }

    override fun fetchTokenFromCache() {

    }
}