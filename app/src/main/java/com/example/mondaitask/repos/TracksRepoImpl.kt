package com.example.mondaitask.repos

import android.graphics.Bitmap
import com.example.mondaitask.Network.Manager.RemoteDataManager
import com.example.mondaitask.Network.Manager.RemoteResponse
import com.example.mondaitask.Network.image_loader.ImageLoader
import com.example.mondaitask.Network.image_loader.ImageLoaderResponse
import com.example.mondaitask.models.TracksResponse
import com.example.mondaitask.utils.AUTH_KEY
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class TracksRepoImpl(val dataManager: RemoteDataManager) : TracksRepo {
    override suspend fun getTracks(
        tracksUrl: String,
        key: String,
        parameters: HashMap<String, String>
    ): Flow<RemoteResponse<TracksResponse>> {
        val headers = HashMap<String, String>()
        headers.put(AUTH_KEY, key)
        return flow {
            emit(
                dataManager.getTracksFromServer(
                    tracksUrl,
                    headers = headers,
                    parameters = parameters
                )
            )
        }.flowOn(Dispatchers.IO)
    }



}