package com.example.mondaitask.Network.Manager

import android.util.JsonReader
import com.example.mondaitask.Network.HTTP.HTTPError
import org.json.JSONObject

sealed class NetworkManagerResponse {
    data class Success(val jsonReader: JsonReader) : NetworkManagerResponse()
    data class Failure(val error: NetworkManagerError) : NetworkManagerResponse()
}