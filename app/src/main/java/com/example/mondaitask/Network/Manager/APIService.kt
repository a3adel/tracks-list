package com.example.mondaitask.Network.Manager

import android.util.JsonReader
import com.example.mondaitask.Network.HTTP.HTTPConnection
import com.example.mondaitask.Network.HTTP.HTTPMethods
import com.example.mondaitask.Network.HTTP.HTTPResponse

import org.json.JSONArray
import org.json.JSONObject
import java.io.InputStreamReader

class APIService private constructor() {
    private val BASE_URL = "http://staging-gateway.mondiamedia.com"
    private var httpConnection: HTTPConnection
    private var basicHeaders: HashMap<String, String>? = null

    init {
        httpConnection = HTTPConnection.getInstance(BASE_URL)
    }

    fun createRequest(
        url: String, headers: HashMap<String, String>? = null,
        parameters: Map<String, String>? = null, method: String = HTTPMethods.GET
    ): NetworkManagerResponse {
        var allHeaders = headers
        if (basicHeaders != null) {
            if (allHeaders != null) {
                for (key in basicHeaders!!.keys) {
                    allHeaders?.put(key, basicHeaders!!.get(key)!!)
                }
            } else
                allHeaders = basicHeaders
        }
        val httpResponse = httpConnection.request(url, allHeaders, parameters, method)
        when (httpResponse) {
            is HTTPResponse.Success -> {
                val inputStreamReader = InputStreamReader(httpResponse.body,"UTF-8")
                val reader = JsonReader(inputStreamReader)



                /**Handle if the response is JSON array **/
                    return NetworkManagerResponse.Success(reader)

            }
            is HTTPResponse.Failure -> {
                return NetworkManagerResponse.Failure(
                    NetworkManagerError(
                        httpResponse.error.errorCode,
                        httpResponse.error.errorMessage
                    )
                )
            }
        }
    }

    fun addBasicHeaders(headers: HashMap<String, String>?): APIService {
        this.basicHeaders = headers
        return this
    }

    companion object {
        private var instance: APIService? = null
        fun getInstance(): APIService {
            if (instance == null)
                instance = APIService()
            return instance as APIService
        }
    }
}