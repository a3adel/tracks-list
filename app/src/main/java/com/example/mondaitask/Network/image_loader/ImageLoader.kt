package com.example.mondaitask.Network.image_loader

import android.graphics.BitmapFactory
import java.net.URL

class ImageLoader {
    fun loadImage(url: String): ImageLoaderResponse {
        return try {
            val inputStream = URL(url).openStream()
            val bitmap = BitmapFactory.decodeStream(inputStream)
            ImageLoaderResponse.Success(bitmap)
        } catch (exc: Exception){
            ImageLoaderResponse.Failure(1,"Error loading the image")
        }
    }
}