package com.example.mondaitask.Network.HTTP

data class HTTPError(val errorCode: Int, val errorMessage: String)