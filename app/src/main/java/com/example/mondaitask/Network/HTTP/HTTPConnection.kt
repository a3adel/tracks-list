package com.example.mondaitask.Network.HTTP

import com.example.mondaitask.utils.SingletonHolder
import com.example.mondaitask.utils.toJsonObject
import java.io.BufferedInputStream
import java.net.HttpURLConnection
import java.net.URL

class HTTPConnection private constructor(val url: String) {

    fun request(
        subUrl: String, headers: Map<String, String>? = null,
        parameters: Map<String, String>? = null, method: String = HTTPMethods.GET
    ): HTTPResponse {
        val fullUrl = url + subUrl
        when (method) {
            HTTPMethods.GET -> return getRequest(fullUrl , headers,parameters)
            HTTPMethods.POST -> {

                return postRequest(fullUrl, headers = headers, parameters = parameters)

            }
            HTTPMethods.PUT -> {

                return putRequest(fullUrl, headers, parameters)

            }
            HTTPMethods.DELETE -> {

                return deleteRequest(fullUrl, headers, parameters)


            }
        }
        throw IllegalArgumentException("Invalid request method")
    }


    private fun getRequest(
        urlPath: String,
        headers: Map<String, String>? = null,
        parameters: Map<String, String>?
    ): HTTPResponse {
        var fullUrl = urlPath

        if (parameters != null) {
            fullUrl = fullUrl+"?"

            for(key in parameters.keys){
                fullUrl = fullUrl+key+"="+parameters.get(key)+"&"
            }
            fullUrl = fullUrl.dropLast(1)
        }
        val url = URL(fullUrl)
        val urlConnection = url.openConnection() as HttpURLConnection
        if (headers != null) {
            for (key in headers.keys) {
                urlConnection.setRequestProperty(key, headers.get(key))
            }
        }

        return try {
            val inputStream = (urlConnection.inputStream)
            urlConnection.disconnect()
            println(urlConnection.responseMessage)
            HTTPResponse.Success(inputStream)
        } catch (exc: Exception) {
            print(urlConnection.responseCode)
            val error = HTTPError(
                errorCode = urlConnection.responseCode,
                errorMessage = urlConnection.responseMessage
            )

            HTTPResponse.Failure(error)
        }
        urlConnection.disconnect()
    }


    private fun postRequest(
        urlPath: String,
        headers: Map<String, String>? = null,
        parameters: Map<String, String>? = null
    ): HTTPResponse {
        val url = URL(urlPath)
        val urlConnection = url.openConnection() as HttpURLConnection
        urlConnection.doOutput = true
        urlConnection.requestMethod = "POST";
        if (headers != null) {
            for (key in headers.keys) {
                urlConnection.setRequestProperty(key, headers.get(key))
            }
        }
        if (parameters != null) {

            val jsonObject = parameters.toJsonObject()

            urlConnection.getOutputStream().use { os ->
                val input = jsonObject.toString().toByteArray()
                os.write(input, 0, input.size)
            }
        }
        return try {
            val inputStream = (urlConnection.inputStream)
            urlConnection.disconnect()
            println(urlConnection.responseMessage)
            HTTPResponse.Success(inputStream)
        } catch (exc: Exception) {
            print(urlConnection.responseCode)
            val error = HTTPError(
                errorCode = urlConnection.responseCode,
                errorMessage = urlConnection.responseMessage
            )
            HTTPResponse.Failure(error)
        }
        urlConnection.disconnect()

    }

    private fun putRequest(
        urlPath: String,
        headers: Map<String, String>? = null,
        parameters: Map<String, String>? = null
    ): HTTPResponse {
        val url = URL(urlPath)
        val urlConnection = url.openConnection() as HttpURLConnection
        urlConnection.doOutput = true
        urlConnection.requestMethod = "PUT";
        if (headers != null) {
            for (key in headers.keys) {
                urlConnection.setRequestProperty(key, headers.get(key))
            }
        }
        if (parameters != null) {


            val jsonObject = parameters.toJsonObject()
            urlConnection.getOutputStream().use { os ->
                val input = jsonObject.toString().toByteArray()
                os.write(input, 0, input.size)
            }
        }
        return try {
            val inputStream = (urlConnection.inputStream)
            urlConnection.disconnect()
            println(urlConnection.responseMessage)
            HTTPResponse.Success(inputStream)
        } catch (exc: Exception) {
            print(urlConnection.responseCode)
            val error = HTTPError(
                errorCode = urlConnection.responseCode,
                errorMessage = urlConnection.responseMessage
            )
            HTTPResponse.Failure(error)
        }
        urlConnection.disconnect()

    }


    private fun deleteRequest(
        urlPath: String,
        headers: Map<String, String>? = null,
        parameters: Map<String, String>? = null
    ): HTTPResponse {
        val url = URL(urlPath)
        val urlConnection = url.openConnection() as HttpURLConnection
        urlConnection.doOutput = true
        urlConnection.requestMethod = "DELETE";
        if (headers != null) {
            for (key in headers.keys) {
                urlConnection.setRequestProperty(key, headers.get(key))
            }
        }
        if (parameters != null) {


            val jsonObject = parameters.toJsonObject()
            urlConnection.getOutputStream().use { os ->
                val input = jsonObject.toString().toByteArray()
                os.write(input, 0, input.size)
            }
        }
        return try {
            val inputStream = (urlConnection.inputStream)
            urlConnection.disconnect()
            println(urlConnection.responseMessage)
            HTTPResponse.Success(inputStream)
        } catch (exc: Exception) {
            print(urlConnection.responseCode)
            val error = HTTPError(
                errorCode = urlConnection.responseCode,
                errorMessage = urlConnection.responseMessage
            )
            HTTPResponse.Failure(error)
        }
        urlConnection.disconnect()

    }

    companion object : SingletonHolder<HTTPConnection, String>(::HTTPConnection)

}