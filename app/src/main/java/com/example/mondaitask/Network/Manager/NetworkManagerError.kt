package com.example.mondaitask.Network.Manager

data class NetworkManagerError(val errorCode: Int, val errorMessage: String)