package com.example.mondaitask.Network.HTTP

object HTTPMethods {
    const val GET = "GET"
    const val POST = "POST"
    const val PUT = "PUT"
    const val DELETE = "DELETE"
}