package com.example.mondaitask.Network.image_loader

import android.graphics.Bitmap

sealed class ImageLoaderResponse(val bitmap: Bitmap?=null) {
    data class Success(val image:Bitmap):ImageLoaderResponse()
    data class Failure(val errorCode:Int, val errorMessage:String):ImageLoaderResponse()
}