package com.example.mondaitask.Network.Manager

import android.util.JsonReader
import com.example.mondaitask.Network.HTTP.HTTPMethods
import com.example.mondaitask.models.Token
import com.example.mondaitask.models.Track
import com.example.mondaitask.models.TracksResponse
import org.json.JSONArray
import org.json.JSONObject

class RemoteDataManager(val apiService: APIService) {
    fun getTokenFromServer(gateWaySubUrl: String): RemoteResponse<Token> {
        val networkManagerResponse =
            apiService.createRequest(gateWaySubUrl, method = HTTPMethods.POST)
        return when (networkManagerResponse) {
            is NetworkManagerResponse.Success -> RemoteResponse.Success(
                Token(
                    networkManagerResponse.jsonReader
                )
            )
            is NetworkManagerResponse.Failure -> RemoteResponse.Error<Token>(
                networkManagerResponse.error.errorCode,
                networkManagerResponse.error.errorMessage
            )
        }

    }

    fun getTracksFromServer(
        tracksUrl: String,
        parameters: HashMap<String, String>,
        headers: HashMap<String, String>
    ): RemoteResponse<TracksResponse> {
        val networkManagerResponse =
            apiService.createRequest(
                tracksUrl,
                method = HTTPMethods.GET,
                parameters = parameters,
                headers = headers
            )
        return when (networkManagerResponse){
            is NetworkManagerResponse.Success -> RemoteResponse.Success(
                TracksResponse(
                    networkManagerResponse.jsonReader
                )
            )
            is NetworkManagerResponse.Failure -> RemoteResponse.Error<TracksResponse>(
                networkManagerResponse.error.errorCode,
                networkManagerResponse.error.errorMessage
            )

        }
    }
}