package com.example.mondaitask.Network.Manager

sealed class RemoteResponse<T> (val data: T? = null){
    data class Success<T>(val response: T) : RemoteResponse<T>()
    data class Error<T>(val errorCode: Int, val errorMessage: String) : RemoteResponse<T>(null)
}