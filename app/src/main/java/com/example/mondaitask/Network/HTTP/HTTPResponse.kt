package com.example.mondaitask.Network.HTTP

import java.io.BufferedInputStream
import java.io.InputStream

sealed class HTTPResponse {
    data class Success(val body:InputStream): HTTPResponse()
    data class Failure(val error: HTTPError): HTTPResponse()
}