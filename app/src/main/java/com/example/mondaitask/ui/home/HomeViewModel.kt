package com.example.mondaitask.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.mondaitask.Network.Manager.RemoteResponse
import com.example.mondaitask.models.Token
import com.example.mondaitask.models.TracksResponse
import com.example.mondaitask.repos.TokenRepo
import com.example.mondaitask.repos.TracksRepo
import com.example.mondaitask.ui.base.BaseViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class HomeViewModel(val repo: TokenRepo, val tracksRepo: TracksRepo) : BaseViewModel() {
    lateinit var token :String
    val tokenLiveDataPrivate = MutableLiveData<RemoteResponse<Token>>()
    val tokenLiveData: LiveData<RemoteResponse<Token>> get() = tokenLiveDataPrivate
    fun getToken() {
        viewModelScope.launch {
            repo.getTokenFromServer().collect {
                tokenLiveDataPrivate.value = it
            }
        }
    }

    val tracksLiveDataPrivate = MutableLiveData<RemoteResponse<TracksResponse>>()
    val tracksLiveData: LiveData<RemoteResponse<TracksResponse>> get() = tracksLiveDataPrivate
    fun getTracks(query: HashMap<String, String>, key: String) {
        viewModelScope.launch {
            tracksRepo.getTracks(parameters = query, key = "Bearer "+key).collect {
                tracksLiveDataPrivate.value = it
            }
        }
    }
}