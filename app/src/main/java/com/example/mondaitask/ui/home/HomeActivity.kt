package com.example.mondaitask.ui.home

import android.os.Bundle
import android.view.View
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mondaitask.MondaiApp
import com.example.mondaitask.Network.Manager.RemoteResponse
import com.example.mondaitask.R
import com.example.mondaitask.models.Token
import com.example.mondaitask.models.TracksResponse
import com.example.mondaitask.repos.TokenRepoImpl
import com.example.mondaitask.repos.TracksRepoImpl
import com.example.mondaitask.ui.base.BaseActivity
import com.example.mondaitask.utils.LIMIT_KEY
import com.example.mondaitask.utils.QUERY_KEY
import com.example.mondaitask.utils.observe
import kotlinx.android.synthetic.main.activity_main.*

class HomeActivity : BaseActivity() {
    lateinit var viewModel: HomeViewModel
    lateinit var tracksAdapter: TracksAdapter
    override fun observeViewModel() {
        observe(viewModel.tokenLiveData, ::handleToken)
        observe(viewModel.tracksLiveData, ::handleTrackes)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
        val tokenRepo = TokenRepoImpl((application as MondaiApp).getRemoteDataManager())
        val tracksRepo = TracksRepoImpl((application as MondaiApp).getRemoteDataManager())
        viewModel = HomeViewModel(tokenRepo, tracksRepo)
        observeViewModel()
        viewModel.getToken()


    }

    fun initView() {
        tracks_sv.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                Toast.makeText(this@HomeActivity, "asdgggad", Toast.LENGTH_LONG).show()
                val searchQuery = tracks_sv.query.toString()
                val query = HashMap<String, String>()
                query.put(QUERY_KEY, searchQuery)
                query.put(LIMIT_KEY, "20")
                viewModel.getTracks(query, viewModel.token)
                return true
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                return false
            }
        })
        tracksAdapter = TracksAdapter()
        val linearLayoutManager = LinearLayoutManager(this)
        track_rv.layoutManager = linearLayoutManager
        track_rv.adapter = tracksAdapter
    }

    fun handleToken(status: RemoteResponse<Token>) {
        when (status) {
            is RemoteResponse.Success -> {
                viewModel.token = status.response.tokenKey
                loading_pb.visibility = View.GONE
            }
            is RemoteResponse.Error -> Toast.makeText(
                this,
                "" + status.errorMessage,
                Toast.LENGTH_LONG
            ).show()

        }
    }

    fun handleTrackes(status: RemoteResponse<TracksResponse>) {
        when (status) {
            is RemoteResponse.Success -> {
                tracksAdapter.tracksList = status.response.tracksList
                tracksAdapter.notifyDataSetChanged()
            }
            is RemoteResponse.Error -> Toast.makeText(
                this,
                "" + status.errorMessage,
                Toast.LENGTH_LONG
            ).show()
        }
    }
}