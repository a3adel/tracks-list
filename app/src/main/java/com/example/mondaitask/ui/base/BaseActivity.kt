package com.example.mondaitask.ui.base

import androidx.appcompat.app.AppCompatActivity

open abstract class BaseActivity: AppCompatActivity() {
    abstract fun observeViewModel()
}