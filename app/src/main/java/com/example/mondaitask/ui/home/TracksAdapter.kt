package com.example.mondaitask.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mondaitask.R
import com.example.mondaitask.models.Track
import kotlinx.android.synthetic.main.item_track.view.*

class TracksAdapter : RecyclerView.Adapter<TracksAdapter.TrackViewHolder>() {
    var tracksList: List<Track>
    init {
        tracksList= ArrayList<Track>()
    }
    class TrackViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrackViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_track, parent, false)
        return TrackViewHolder(view)
    }

    override fun getItemCount(): Int {
        return tracksList.size
    }

    override fun onBindViewHolder(holder: TrackViewHolder, position: Int) {
        holder.itemView.track_name.text = tracksList.get(position).title
        holder.itemView.track_artist.text = tracksList.get(position).artist.name
        val genresBuilder: StringBuilder = java.lang.StringBuilder()

        for (genre in tracksList.get(position).genres)
            genresBuilder.append(",$genre").toString()
        holder.itemView.track_genres.text = genresBuilder.toString()
    }
}