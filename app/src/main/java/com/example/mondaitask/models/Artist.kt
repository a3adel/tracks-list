package com.example.mondaitask.models

import android.util.JsonReader

const val ID_KEY = "id"
const val NAME_KEY = "name"

data class Artist(val jsonReader: JsonReader) {
    lateinit var name: String
    var id: Int = 0

    init {
        jsonReader.beginObject()
        while (jsonReader.hasNext()) {
            val key = jsonReader.nextName()
            if (key == ID_KEY)
                id = jsonReader.nextInt()
            else if (key == NAME_KEY)
                name = jsonReader.nextString()
            else
                jsonReader.skipValue()
        }
        jsonReader.endObject()


    }


}