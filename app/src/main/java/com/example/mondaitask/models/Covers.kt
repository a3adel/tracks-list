package com.example.mondaitask.models

import android.util.JsonReader
import org.json.JSONObject

const val MEDIUM_COVER_KEY = "medium"
const val SMALL_COVER_KEY = "small"
const val LARGE_COVER_KEY = "large"

data class Covers(val jsonReader: JsonReader) {
    lateinit var small: String
    lateinit var medium: String
    lateinit var large: String

    init {
        jsonReader.beginObject()
        while (jsonReader.hasNext()){
            val key = jsonReader.nextName()
            if(key == MEDIUM_COVER_KEY)
                medium = jsonReader.nextString()
            else if(key == LARGE_COVER_KEY)
                large = jsonReader.nextString()
            else if(key == SMALL_COVER_KEY)
                small = jsonReader.nextString()
            else
                jsonReader.skipValue()
        }
        jsonReader.endObject()

    }

}