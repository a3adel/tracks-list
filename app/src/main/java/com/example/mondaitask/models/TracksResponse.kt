package com.example.mondaitask.models

import android.util.JsonReader
import org.json.JSONArray

data class TracksResponse(val jsonReader: JsonReader) {
    val tracksList:List<Track>
    init {
        tracksList = ArrayList()
        jsonReader.beginArray()
        while (jsonReader.hasNext())
            tracksList.add(Track(jsonReader))
        jsonReader.endArray()


    }
}