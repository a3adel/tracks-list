package com.example.mondaitask.models

import android.util.JsonReader

const val TOKEN_KEY = "accessToken"
const val TOKEN_TYPE_KEY = "tokenType"
const val EXPIRE_KEY = "expiresIn"

data class Token(val reader: JsonReader) {
    lateinit var tokenKey: String
    lateinit var tokenType: String
    var expire: Int = -1

    init {
        reader.beginObject()
        while (reader.hasNext()) {
            val key = reader.nextName()
            if (key == TOKEN_KEY)
                tokenKey = reader.nextString()
            else if (key == TOKEN_TYPE_KEY) {
                tokenType = reader.nextString()

            } else if (key == EXPIRE_KEY)
                expire = reader.nextInt()
            else
                reader.skipValue()
        }
        reader.endObject()
    }
}