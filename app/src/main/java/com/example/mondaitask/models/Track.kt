package com.example.mondaitask.models

import android.util.JsonReader

const val TRACK_ID_KEY = "id"
const val TRACK_GENRE_KEY = "genres"
const val TRACK_COVERS_KEY = "cover"
const val TRACK_ARTIST_KEY = "mainArtist"
const val TRACK_TITLE_KEY = "title"

data class Track(val jsonReader: JsonReader) {
    var id: Int = 0
    lateinit var covers: Covers
    lateinit var artist: Artist
    var genres: ArrayList<String>
    lateinit var title: String

    init {
        genres = ArrayList()
        jsonReader.beginObject()
        while (jsonReader.hasNext()) {
            val key = jsonReader.nextName()
            if (key == TRACK_ID_KEY)
                id = jsonReader.nextInt()
            else if (key == TRACK_COVERS_KEY)
                covers = Covers(jsonReader)
            else if (key == TRACK_ARTIST_KEY)
                artist = Artist(jsonReader)
            else if (key == TRACK_GENRE_KEY)
                genres = readStringArray(jsonReader)
            else if (key == TRACK_TITLE_KEY)
                title = jsonReader.nextString()
            else
                jsonReader.skipValue()
        }
        jsonReader.endObject()

    }

    private fun readStringArray(jsonReader: JsonReader): java.util.ArrayList<String> {
        val stringsList = ArrayList<String>()
        jsonReader.beginArray()
        while (jsonReader.hasNext())
            stringsList.add(jsonReader.nextString())
        jsonReader.endArray()
        return stringsList
    }

}
