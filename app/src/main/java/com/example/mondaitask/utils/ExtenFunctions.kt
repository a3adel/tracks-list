package com.example.mondaitask.utils

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import org.json.JSONObject
import java.io.InputStream
import java.nio.charset.Charset



fun <T>Map<String,T>.toJsonObject():JSONObject{
    val jsonObject = JSONObject()
    for (key in this.keys) {
        jsonObject.put(key, this.get(key))
    }
    return jsonObject
}

fun <T> LifecycleOwner.observe(liveData: LiveData<T>, action: (t: T) -> Unit) {
    liveData.observe(this, Observer { it?.let { t -> action(t) } })
}

