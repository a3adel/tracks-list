package com.example.mondaitask.utils

const val CONTENT_TYPE_KEY = "Content-Type"
const val GATE_WAY_KEY_KEY = "X-MM-GATEWAY-KEY"
const val AUTH_KEY = "Authorization"

const val QUERY_KEY = "query"
const val LIMIT_KEY = "limit"